#include<iostream>
using namespace std;

int count_node = 0;

//defining a node
class node
{
    public:
    int data;
    node *next;
};

//head node
node *head = new node();

//Inserting a node
void Insert( int data , int n )
{
    if( n > 0 && n <= count_node + 1)
    {
    node *new_node = new node();
    new_node->data = data;
    new_node->next = NULL;
    if( n == 1)
    {
        //literally making a new node
        new_node->next = head;
        head = new_node;
        count_node++;
        return;
    }
    node* temp = new node();
    temp = head;
    for( int i = 0; i < n - 2; i++ )
    {
        temp = temp->next;
    }
    new_node->next = temp->next;
    temp->next = new_node;

    count_node++;
    return;
    }
    else
    {
        cout<<"invalid index\n";
    }
}

void Reverse()
{
    node *current_node = new node();
    node* previous_node = new node();
    node *next_node = new node();

    current_node = head;
    previous_node = NULL;
    next_node = NULL;
    while( current_node != NULL )
    {
        next_node = current_node->next;
        current_node->next = previous_node;
        previous_node = current_node;
        current_node = next_node;
    }
    head = previous_node;
}

//printing a node
void PrintList()
{
    node *next_node = new node();
    next_node = head;
    cout<<"List is : ";
    while( next_node != NULL )
    {
        cout<<next_node->data<<" ";
        next_node = next_node->next;
    }
    cout<<endl;
}

//Run
int main()
{
    head = NULL;
    Insert( 1,1 );
    Insert( 3,2 );
    Insert( 4,1 );
    Insert( 5,2 );
    PrintList();
    Reverse();
    PrintList();
}
